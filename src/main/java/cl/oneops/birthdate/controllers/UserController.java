package cl.oneops.birthdate.controllers;

import javax.validation.Valid;

import cl.oneops.birthdate.dto.*;
import cl.oneops.birthdate.services.service.BirthdayProcessorService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;

@RequestMapping(path="/user")
@RestController
@Api(value = "/user",tags = {"user"}, description = "user management controller")
public class UserController {
	
	@Autowired
	private BirthdayProcessorService birthdayProcessor;
	
	@PostMapping(path="/birthdate")
	@ApiOperation(value = "Verify the user birthday and congratulate", notes = "Return user firstname, lastname, age and days to the next birthday "
			+ "and congratulates with a poem if the birthday is today")
	public ResponseEntity<?> getProjectsBy(
			@ApiParam(value = "JSON with format {\"firstname\":\"string\", \"lastname\":\"string\", \"birthdate\":\"date DD-MM-YYYY\"}", 
					examples = @Example(value = { 
							@ExampleProperty(value="{'firstname': 'person', 'lastname':'mcreal', 'birthdate':'15-01-1990'}", 
									mediaType = "application/json") 
							}),
					required = true)
	        @Valid @RequestBody BirthdayCongratsInputDto param
	){
		
		BirthdayCongratsOutputDto result = birthdayProcessor.processUserInfo(param);
	    
		return new ResponseEntity<BirthdayCongratsOutputDto>(result, HttpStatus.OK);
	}
}
