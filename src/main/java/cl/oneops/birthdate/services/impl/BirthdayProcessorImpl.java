package cl.oneops.birthdate.services.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cl.oneops.birthdate.dto.BirthdayCongratsInputDto;
import cl.oneops.birthdate.dto.BirthdayCongratsOutputDto;
import cl.oneops.birthdate.helper.BirthdateHelper;
import cl.oneops.birthdate.helper.RandomPoemHelper;
import cl.oneops.birthdate.helper.StringProcessorHelper;
import cl.oneops.birthdate.helper.ValidatorHelper;
import cl.oneops.birthdate.services.service.BirthdayProcessorService;

@Service
public class BirthdayProcessorImpl implements BirthdayProcessorService {

	@Value("${cl.oneops.birthdate.congratulation}")
    private String CONGRATULATION_MESSAGE;
	
	@Override
	public BirthdayCongratsOutputDto processUserInfo(BirthdayCongratsInputDto param) {
		//Check validity of BirthdayCongratsInputDto parameter
		ValidatorHelper.verifyBirthdayCongratsInputDto(param);
		
		BirthdayCongratsOutputDto result = new BirthdayCongratsOutputDto();
		
		result.setFirstname(
				StringProcessorHelper.getCapitalicedFirstWordOf(
						param.getFirstname()
					)
				);
		
		result.setLastname(
				StringProcessorHelper.getCapitalicedFirstWordOf(
						param.getLastname()
					)
				);
		
		result.setAge(
				BirthdateHelper.getAgeFromDate(
						param.getBirthdate()	
					)
				);
		
		result.setDaysToNextBirthday(
				BirthdateHelper.getDaysToNextBirthday(
						param.getBirthdate()	
					)
				);
		
		result.setBirthdate(
				BirthdateHelper.parseDateToDDMMYY(
						param.getBirthdate()
					)
				);
		
		//If birthday is today get a random poem and congratulation message
		if (result.getDaysToNextBirthday().equals(0)) {
			result.setDaysToNextBirthday(null);
			result.setPoem(RandomPoemHelper.get());
			result.setCongratulation(CONGRATULATION_MESSAGE);
		}
		
		return result;
	}
	
	
	
}