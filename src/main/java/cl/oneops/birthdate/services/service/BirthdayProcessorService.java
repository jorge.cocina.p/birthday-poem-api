package cl.oneops.birthdate.services.service;

import cl.oneops.birthdate.dto.BirthdayCongratsInputDto;
import cl.oneops.birthdate.dto.BirthdayCongratsOutputDto;

public interface BirthdayProcessorService {
	
	BirthdayCongratsOutputDto processUserInfo(BirthdayCongratsInputDto param);
	
}
