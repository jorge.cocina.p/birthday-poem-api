package cl.oneops.birthdate.dto;

public class BirthdayCongratsOutputDto {

	private String firstname;
	private String lastname;
	private String birthdate;
	private Integer age;
	private Integer daysToNextBirthday;
	private String congratulation;
	private RandomPoemDto poem;
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getDaysToNextBirthday() {
		return daysToNextBirthday;
	}
	public void setDaysToNextBirthday(Integer daysToNextBirthdate) {
		this.daysToNextBirthday = daysToNextBirthdate;
	}
	public String getCongratulation() {
		return congratulation;
	}
	public void setCongratulation(String congratulation) {
		this.congratulation = congratulation;
	}
	public RandomPoemDto getPoem() {
		return poem;
	}
	public void setPoem(RandomPoemDto poem) {
		this.poem = poem;
	}
	
}
