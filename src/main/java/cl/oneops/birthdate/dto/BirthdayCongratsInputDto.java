package cl.oneops.birthdate.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class BirthdayCongratsInputDto {

	@NotBlank(message = "firstname can not be null or empty")
	@NotNull(message = "firstname can not be null or empty")
	private String firstname;
	
	@NotBlank(message = "lastname can not be null or empty")
	@NotNull(message = "lastname can not be null or empty")
	private String lastname;
	
	@NotBlank(message = "birthdate can not be null or empty")
	@NotNull(message = "birthdate can not be null or empty")
	private String birthdate;
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	
}
