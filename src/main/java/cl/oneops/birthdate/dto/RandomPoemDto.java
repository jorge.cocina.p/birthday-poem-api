package cl.oneops.birthdate.dto;

public class RandomPoemDto {
	
	private String title;
	private String content;
	private String url;
	private RandomPoemPoetDto poet;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public RandomPoemPoetDto getPoet() {
		return poet;
	}
	public void setPoet(RandomPoemPoetDto poet) {
		this.poet = poet;
	}
	
}
