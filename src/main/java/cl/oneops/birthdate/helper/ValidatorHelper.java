package cl.oneops.birthdate.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cl.oneops.birthdate.dto.BirthdayCongratsInputDto;
import cl.oneops.birthdate.exception.BusinessLogicException;

public final class ValidatorHelper {



	protected ValidatorHelper() {
		super();
	}
	
	/**
	 * Check the format of param for processUserInfo
	 * @param param BirthdayCongratsInputDto object
	 * @param firstname	*required
	 * @param lastname	*required
	 * @param birthdate	*required format dd-mm-yyyy
	 */
	public static void verifyBirthdayCongratsInputDto(BirthdayCongratsInputDto param) {
		
		List<String> fields = new ArrayList<String>();
		
		if (param.getFirstname() == null || param.getFirstname().trim().isEmpty()) {
			fields.add("firstname can not be null or empty");
		}
		
		if (param.getLastname() == null || param.getLastname().trim().isEmpty()) {
			fields.add("lastname can not be null or empty");
		}
		
		if (
				param.getBirthdate() == null 
				|| param.getBirthdate().trim().isEmpty()
			) {
			fields.add("birthdate can not be null or empty");
		} else {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			try {
				Date date = formatter.parse(param.getBirthdate());
				Date now = new Date();
				
				if (now.before(date)) {
					fields.add("birthdate can not be a future date");
				}
			} catch (ParseException e) {
				fields.add("birthdate incorrect format (expected dd-mm-yyyy)");
			}
		}
		
		if (fields.size() != 0) {
			throw new BusinessLogicException(MessageErrorHelper.dataValidationInvalid(fields));
		}
	}

}
