package cl.oneops.birthdate.helper;

import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class MessageSuccessHelper {

    /**
     * Instantiates a new message success helper.
     */
    private MessageSuccessHelper(){
        super();
    }

    /**
     * Message success.
     *
     * @param message the description
     * @return the response entity
     */
    public static final String success(String message) {

        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(ConstantHelper.KEY_MAP_INDEX, 1);
        error.put(ConstantHelper.KEY_MAP_SUCCESS, true );
        error.put(ConstantHelper.KEY_MAP_DESCRIPTION, message );
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);

    }
}
