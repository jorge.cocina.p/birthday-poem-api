package cl.oneops.birthdate.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.validation.FieldError;

import com.google.gson.GsonBuilder;

public final class MessageErrorHelper {
	
	/**
     * Instantiates a new message error helper.
     */
    private MessageErrorHelper(){
        super();
    }

    public static final String businessException(String description) {
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(ConstantHelper.KEY_MAP_INDEX, ConstantHelper.KEY_BUSINESS);
        error.put(ConstantHelper.KEY_MAP_VALUE, ConstantHelper.VAL_BUSINESS);
        error.put(ConstantHelper.KEY_MAP_DESCRIPTION, description);
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }

    public static final String nullPointerException() {
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(ConstantHelper.KEY_MAP_INDEX, ConstantHelper.KEY_ENTITY_NULL_POINTER);
        error.put(ConstantHelper.KEY_MAP_VALUE, ConstantHelper.VAL_NULL_POINTER);
        error.put(ConstantHelper.KEY_MAP_DESCRIPTION, ConstantHelper.DSC_NULL_POINTER);
        error.put(ConstantHelper.KEY_MAP_DETAIL, ConstantHelper.DET_NULL_POINTER);
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }
    
    public static final String internalError(Exception exception) {
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(ConstantHelper.KEY_MAP_INDEX, ConstantHelper.KEY_INTERNAL_ERROR);
        error.put(ConstantHelper.KEY_MAP_VALUE, ConstantHelper.VAL_INTERNAL_ERROR);
        error.put(ConstantHelper.KEY_MAP_DESCRIPTION, exception.getMessage());
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }
    
    public static final String internalError(String exception) {
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(ConstantHelper.KEY_MAP_INDEX, ConstantHelper.KEY_INTERNAL_ERROR);
        error.put(ConstantHelper.KEY_MAP_VALUE, ConstantHelper.VAL_INTERNAL_ERROR);
        error.put(ConstantHelper.KEY_MAP_DESCRIPTION, exception);
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }

    public static final String dataRequestInvalid(String message) {
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(ConstantHelper.KEY_MAP_INDEX, ConstantHelper.KEY_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_VALUE, ConstantHelper.VAL_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_DESCRIPTION, ConstantHelper.DSC_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_DETAIL, (null != message ? message : ConstantHelper.EMPTY) );
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }

    public static final String dataValidationInvalid(String string ) {
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(ConstantHelper.KEY_MAP_INDEX, ConstantHelper.KEY_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_VALUE, ConstantHelper.VAL_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_DESCRIPTION, ConstantHelper.DSC_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_DETAIL, (null != string ? string : new ArrayList<>()) );
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }
    
    public static final String dataValidationInvalid(List<String> fields ) {
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(ConstantHelper.KEY_MAP_INDEX, ConstantHelper.KEY_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_VALUE, ConstantHelper.VAL_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_DESCRIPTION, ConstantHelper.DSC_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_DETAIL, (null != fields && fields.size() != 0 ? fields : new ArrayList<>()) );
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }
    
    public static final String dataValidationInvalidErrors(List<FieldError> fields ) {
    	List<String> details = new ArrayList<String>();
    	for (FieldError field: fields) {
    		details.add(field.getDefaultMessage());
    	}
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(ConstantHelper.KEY_MAP_INDEX, ConstantHelper.KEY_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_VALUE, ConstantHelper.VAL_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_DESCRIPTION, ConstantHelper.DSC_DATA_INVALID);
        error.put(ConstantHelper.KEY_MAP_DETAIL, details );
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }
}
