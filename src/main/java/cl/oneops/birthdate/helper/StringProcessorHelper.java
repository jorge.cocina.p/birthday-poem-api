package cl.oneops.birthdate.helper;

import org.apache.commons.lang3.StringUtils;

public final class StringProcessorHelper {

	/**
	 * Return the first word of the string, capitalized
	 * @param words
	 * @return string ej: "hellO World!" => "Hello"
	 */
	public static String getCapitalicedFirstWordOf(String words) {
		
		if (words == null) {
			return "";
		}
		
		String[] splited = words.split(" ");
		
		if (splited == null || splited.length == 0) {
			return "";
		}
		
		return StringUtils.capitalize(splited[0].toLowerCase());
		
	}
	
}
