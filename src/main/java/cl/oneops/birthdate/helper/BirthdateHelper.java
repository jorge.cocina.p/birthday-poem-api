package cl.oneops.birthdate.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public final class BirthdateHelper {

	/**
	 * Return the actual age from a birthdate input
	 * @param date birthdate in format dd-mm-yyyy
	 * @return Integer age | null on error
	 */
	public static Integer getAgeFromDate(String date) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		LocalDate now = LocalDate.now();
		LocalDate birthdate = null;
		Date birthdateDate = null;
		
		try {
			birthdateDate = formatter.parse(date);
			Instant instant = birthdateDate.toInstant();
			ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
			birthdate = zdt.toLocalDate();
			if (now.isBefore(birthdate)) {
				return null;
			}
		} catch (ParseException e) {
			return null;
		}
		Period period = Period.between(birthdate, now);
		int diff = Math.abs(period.getYears());
		return Integer.valueOf(diff);
		
	}
	
	/**
	 * Conver date from format dd-mm-yyyy to dd/mm/yy
	 * @param date 
	 * @return Data in format dd/mm/yy | null on error
	 */
	public static String parseDateToDDMMYY(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		LocalDate birthdate = null;
		Date birthdateDate = null;
		
		try {
			birthdateDate = formatter.parse(date);
			Instant instant = birthdateDate.toInstant();
			ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
			birthdate = zdt.toLocalDate();
		} catch (ParseException e) {
			return null;
		}
		
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yy");
		return birthdate.format(format);
	}
	
	/**
	 * Return quantity of days to the next birthday from birthdate input
	 * @param date birthdate in format dd-mm-yyyy
	 * @return Integer days to the next birthday | null on error | 0 if birthday its today
	 */
	public static Integer getDaysToNextBirthday(String date) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		LocalDate now = LocalDate.now();
		LocalDate birthdate = null;
		LocalDate birthdateOriginal = null;
		Date birthdateDate = null;
		
		try {
			String[] separatedDate = date.split("-");
			int year = now.getYear();
			
			birthdateDate = formatter.parse(date);
			
			Instant instant = birthdateDate.toInstant();
			ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
			birthdateOriginal = zdt.toLocalDate();
			
			date = separatedDate[0] + "-" + separatedDate[1] + "-" + year;
			
			birthdateDate = formatter.parse(date);
			
			instant = birthdateDate.toInstant();
			zdt = instant.atZone(ZoneId.systemDefault());
			birthdate = zdt.toLocalDate();
			if (now.isBefore(birthdateOriginal)) {
				return null;
			}
			//If next birthday is next year 
			if (now.isAfter(birthdate) || now.isEqual(birthdateOriginal)) {
				year+=1;
				date = separatedDate[0] + "-" + separatedDate[1] + "-" + year;
				
				birthdateDate = formatter.parse(date);
				
				instant = birthdateDate.toInstant();
				zdt = instant.atZone(ZoneId.systemDefault());
				birthdate = zdt.toLocalDate();
			} else if (now.isEqual(birthdate)) {
				return 0;
			}
		} catch (ParseException e) {
			return null;
		}
		
		long days = ChronoUnit.DAYS.between(now, birthdate);
		return Math.abs(Math.toIntExact(days));
		
	}
	
}
