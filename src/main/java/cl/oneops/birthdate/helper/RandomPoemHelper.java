package cl.oneops.birthdate.helper;

import java.util.Random;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import cl.oneops.birthdate.dto.RandomPoemDto;
import cl.oneops.birthdate.exception.BusinessLogicException;

public final class RandomPoemHelper {
	
	private static String URI_POEMS = "https://www.poemist.com/api/v1/randompoems";
	
	public static RandomPoemDto get() {
		
		RestTemplate restTemplate = new RestTemplate();
		RandomPoemDto[] poems = null;
		
		try {
			poems = restTemplate.getForObject(URI_POEMS, RandomPoemDto[].class);
		} catch (RestClientException e) {
			throw new BusinessLogicException(MessageErrorHelper.businessException("Error on connection with " + URI_POEMS)); 
		}
		
		Random rand = new Random();
		if (poems == null || poems.length == 0) {
			throw new BusinessLogicException(MessageErrorHelper.businessException("Random poems not found"));
		}
		
		return poems[rand.nextInt(poems.length)];
		
	}
	
}
