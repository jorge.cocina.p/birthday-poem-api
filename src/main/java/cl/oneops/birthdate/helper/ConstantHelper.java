package cl.oneops.birthdate.helper;

public final class ConstantHelper {
	
	/** The Constant EMPTY. */
	public static final String EMPTY 							= "";
	
	/** Key's Constants. */
	public static final String KEY_MAP_INDEX             		= "id";
	public static final String KEY_MAP_VALUE             		= "value";
	public static final String KEY_MAP_DESCRIPTION         		= "description";
	public static final String KEY_MAP_DETAIL            		= "detail";
	public static final String KEY_MAP_SUCCESS            		= "success";
	
	/** Database Exception values. */
	public static final Integer KEY_INTERNAL_SERVER_EXCEPTION 	= -1;
	public static final String VAL_INTERNAL_SERVER_EXCEPTION 	= "InternalServerError";
	public static final String DSC_INTERNAL_SERVER_EXCEPTION 	= "An unwarranted error has occurred";
	
	/** General error in business logic. */
	public static final Integer KEY_BUSINESS					= -2;
	public static final String VAL_BUSINESS 					= "Business Logic exception";

	/** Entity null pointer */
	public static final Integer KEY_ENTITY_NULL_POINTER			= -3;
	public static final String VAL_NULL_POINTER 			    = "NullPointerException";
	public static final String DSC_NULL_POINTER 			    = "Invalid Data";
	public static final String DET_NULL_POINTER			        = "Data in request is null";

	/** Entity data not valid */
	public static final Integer KEY_INTERNAL_ERROR		        = -4;
	public static final String VAL_INTERNAL_ERROR			    = "InternalExceptionError";
	public static final String DSC_INTERNAL_ERROR		        = "Internal error";

	/**Invalid data request**/
	public static final Integer KEY_DATA_INVALID		    	= -5;
	public static final String  VAL_DATA_INVALID				= "DataNotValidException";
	public static final String  DSC_DATA_INVALID				= "Invalid data";
	public static final String  DET_DATA_INVALID 		    	= "invalid request data";

	/** The Constant EMPTY RESPONSE. */
	public static final String EMPTY_RESPONSE                   = "[]";

	/**
	 * Instantiates a new constants.
	 */
	private ConstantHelper() {
		super();
	}


}
