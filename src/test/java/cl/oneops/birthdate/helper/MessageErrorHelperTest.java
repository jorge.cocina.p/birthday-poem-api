package cl.oneops.birthdate.helper;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class MessageErrorHelperTest {

	@Test
	public void testBusinessException() {
	
		String message = "test message";
		String result = MessageErrorHelper.businessException(message);
		
		assertNotNull(result);
		assertTrue(result.contains(message));
	}
	
	@Test
	public void testNullPointerException() {
	
		String result = MessageErrorHelper.nullPointerException();
		
		assertNotNull(result);
	}
	
	@Test
	public void testInternalException() {
	
		String message = "test message";
		String result = MessageErrorHelper.internalError(message);
		
		assertNotNull(result);
		assertTrue(result.contains(message));
	}
	
	@Test
	public void testInternalExceptionError() {
	
		String message = "test message";
		String result = MessageErrorHelper.internalError(new Exception(message));
		
		assertNotNull(result);
		assertTrue(result.contains(message));
	}
	
	@Test
	public void testDataRequestInvalidException() {
	
		String message = "test message";
		String result = MessageErrorHelper.dataRequestInvalid(message);
		
		assertNotNull(result);
		assertTrue(result.contains(message));
	}
	
	@Test
	public void testDataValidationInvalidException() {
	
		String message = "test message";
		String result = MessageErrorHelper.dataValidationInvalid(message);
		
		assertNotNull(result);
		assertTrue(result.contains(message));
	}
	
}
