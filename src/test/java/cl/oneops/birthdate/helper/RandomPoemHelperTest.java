package cl.oneops.birthdate.helper;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import cl.oneops.birthdate.dto.RandomPoemDto;

@RunWith(SpringRunner.class)
public class RandomPoemHelperTest {

	@Test
	public void getPoem() {
		
		RandomPoemDto poem = RandomPoemHelper.get();
		assertNotNull(poem);
	}
}
