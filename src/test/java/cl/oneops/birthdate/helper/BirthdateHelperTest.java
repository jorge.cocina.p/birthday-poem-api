package cl.oneops.birthdate.helper;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
public class BirthdateHelperTest {

	@Test
	public void checkAge() {
		LocalDate now = LocalDate.now();
		
		Integer ageToTest1 = 1;
		Integer ageToTest2 = 70;
		Integer ageToTest3 = 45;
		
		String date1 = now.getDayOfMonth() + "-" + now.getMonthValue() + "-" + (now.getYear() - ageToTest1);
		String date2 = now.getDayOfMonth() + "-" + now.getMonthValue() + "-" + (now.getYear() - ageToTest2);
		String date3 = now.getDayOfMonth() + "-" + now.getMonthValue() + "-" + (now.getYear() - ageToTest3);
		
		Integer age1 = BirthdateHelper.getAgeFromDate(date1);
		assertNotNull(age1);
		assertTrue(age1.equals(ageToTest1));
		
		Integer age2 = BirthdateHelper.getAgeFromDate(date2);
		assertNotNull(age2);
		assertTrue(age2.equals(ageToTest2));
		
		Integer age3 = BirthdateHelper.getAgeFromDate(date3);
		assertNotNull(age3);
		assertTrue(age3.equals(ageToTest3));
		
		Integer age4 = BirthdateHelper.getAgeFromDate("asd-asd-asd");
		assertNull(age4);
		
		now = now.plusDays(1);
		String date4 = now.getDayOfMonth() + "-" + now.getMonthValue() + "-" + now.getYear();
		Integer age5 = BirthdateHelper.getAgeFromDate(date4);
		assertNull(age5);
		
	}
	
	@Test
	public void checkParseDate() {
		LocalDate now = LocalDate.now();
		
		String date = "05-03-" + (now.getYear()-1);
		String expected = "05/03/" + ((now.getYear() - 1) % 100);
		
		date = BirthdateHelper.parseDateToDDMMYY(date);
		
		assertNotNull(date);
		assertTrue(date.equals(expected));
		
		date = BirthdateHelper.parseDateToDDMMYY("asd");
		assertNull(date);
	}
	
	@Test
	public void checkCountDays() {
		LocalDate now = LocalDate.now();
		now = now.plusDays(1);
		
		String date = now.getDayOfMonth() + "-" + now.getMonthValue() + "-" + (now.getYear()-1);
		Integer expected = 1;
		
		Integer days = BirthdateHelper.getDaysToNextBirthday(date);
		assertNotNull(days);
		assertTrue(days.equals(expected));
		
		days = BirthdateHelper.getDaysToNextBirthday("adsasd");
		assertNull(days);
		
		now = LocalDate.now();
		now = now.plusDays(1);
		date = now.getDayOfMonth() + "-" + now.getMonthValue() + "-" + now.getYear();
		days = BirthdateHelper.getDaysToNextBirthday(date);
		assertNull(days);
		
	}
}
