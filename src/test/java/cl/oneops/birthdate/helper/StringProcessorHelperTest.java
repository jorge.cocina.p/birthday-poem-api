package cl.oneops.birthdate.helper;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class StringProcessorHelperTest {

	@Test
	public void processOk() {
		
		String text = "aSdD wq easda dq";
		String expected = "Asdd";
		
		text = StringProcessorHelper.getCapitalicedFirstWordOf(text);
		
		assertNotNull(text);
		assertTrue(text.equals(expected));
		
	}
	
}
