package cl.oneops.birthdate.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.google.gson.Gson;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cl.oneops.birthdate.BirthdateApplication;
import cl.oneops.birthdate.dto.BirthdayCongratsInputDto;

@RunWith(SpringRunner.class)
@SpringBootTest( classes = BirthdateApplication.class)
@AutoConfigureMockMvc
public class UserControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Value("${cl.oneops.birthdate.congratulation}")
    private String CONGRATULATION_MESSAGE;
	
	@Test
    public void returnNormal() throws Exception {

		
        LocalDate now = LocalDate.now();
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setFirstname("person");
        input.setLastname("mcreal");
        Gson gson = new Gson();
        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        
        input.setBirthdate(now.format(format));
        
		this.mockMvc.perform(MockMvcRequestBuilders.post("/user/birthdate")
				.contentType(MediaType.APPLICATION_JSON)
				.content(gson.toJson(input))
				.characterEncoding("utf-8")
				.accept(MediaType.APPLICATION_JSON))
        		.andExpect(status().isOk())
        		.andExpect(jsonPath("$.firstname").value("Person"))
        		.andExpect(jsonPath("$.congratulation").isEmpty());

    }
	
	@Test
    public void returnBirthdate() throws Exception {
		
        LocalDate date = LocalDate.now();
        date = date.minusYears(1);
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setFirstname("person totaly");
        input.setLastname("mcreal");
        Gson gson = new Gson();
        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        
        input.setBirthdate(date.format(format));

        assertNotNull(CONGRATULATION_MESSAGE);
        assertTrue(!CONGRATULATION_MESSAGE.equals(""));
		this.mockMvc.perform(MockMvcRequestBuilders.post("/user/birthdate")
				.contentType(MediaType.APPLICATION_JSON)
				.content(gson.toJson(input))
				.characterEncoding("utf-8")
				.accept(MediaType.APPLICATION_JSON))
        		.andExpect(status().isOk())
        		.andExpect(jsonPath("$.firstname").value("Person"))
        		.andExpect(jsonPath("$.congratulation").value(CONGRATULATION_MESSAGE));

    }
	
	@Test
    public void firstnameEmpty() throws Exception {
		
        LocalDate date = LocalDate.now();
        date = date.minusYears(1);
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setFirstname("");
        input.setLastname("mcreal");
        Gson gson = new Gson();
        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        
        input.setBirthdate(date.format(format));
        
		this.mockMvc.perform(MockMvcRequestBuilders.post("/user/birthdate")
				.contentType(MediaType.APPLICATION_JSON)
				.content(gson.toJson(input))
				.characterEncoding("utf-8")
				.accept(MediaType.APPLICATION_JSON))
        		.andExpect(status().is(400))
        		.andExpect(jsonPath("$.id").value(-5))
        		.andExpect(jsonPath("$.value").value("DataNotValidException"));

    }
	
	@Test
    public void lastnameEmpty() throws Exception {
		
        LocalDate date = LocalDate.now();
        date = date.minusYears(1);
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setFirstname("person");
        input.setLastname("");
        Gson gson = new Gson();
        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        
        input.setBirthdate(date.format(format));
        
		this.mockMvc.perform(MockMvcRequestBuilders.post("/user/birthdate")
				.contentType(MediaType.APPLICATION_JSON)
				.content(gson.toJson(input))
				.characterEncoding("utf-8")
				.accept(MediaType.APPLICATION_JSON))
        		.andExpect(status().is(400))
        		.andExpect(jsonPath("$.id").value(-5))
        		.andExpect(jsonPath("$.value").value("DataNotValidException"));

    }
	
	@Test
    public void birthdateEmpty() throws Exception {
		
		LocalDate date = LocalDate.now();
        date = date.plusDays(1);
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setFirstname("person");
        input.setLastname("mcreal");
        Gson gson = new Gson();
        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        
        input.setBirthdate(date.format(format));
        
		this.mockMvc.perform(MockMvcRequestBuilders.post("/user/birthdate")
				.contentType(MediaType.APPLICATION_JSON)
				.content(gson.toJson(input))
				.characterEncoding("utf-8")
				.accept(MediaType.APPLICATION_JSON))
        		.andExpect(status().is(400))
        		.andExpect(jsonPath("$.id").value(-5))
        		.andExpect(jsonPath("$.value").value("DataNotValidException"));

    }
	
	@Test
    public void birthdateFuture() throws Exception {
		
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setFirstname("person");
        input.setLastname("mcreal");
        Gson gson = new Gson();
        
		this.mockMvc.perform(MockMvcRequestBuilders.post("/user/birthdate")
				.contentType(MediaType.APPLICATION_JSON)
				.content(gson.toJson(input))
				.characterEncoding("utf-8")
				.accept(MediaType.APPLICATION_JSON))
        		.andExpect(status().is(400))
        		.andExpect(jsonPath("$.id").value(-5))
        		.andExpect(jsonPath("$.value").value("DataNotValidException"));

    }
	
}
