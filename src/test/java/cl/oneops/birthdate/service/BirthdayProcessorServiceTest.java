package cl.oneops.birthdate.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cl.oneops.birthdate.BirthdateApplication;
import cl.oneops.birthdate.dto.BirthdayCongratsInputDto;
import cl.oneops.birthdate.dto.BirthdayCongratsOutputDto;
import cl.oneops.birthdate.exception.BusinessLogicException;
import cl.oneops.birthdate.services.service.BirthdayProcessorService;

@RunWith(SpringRunner.class)
@SpringBootTest( classes = BirthdateApplication.class)
@AutoConfigureMockMvc
public class BirthdayProcessorServiceTest {

	@Autowired
	private BirthdayProcessorService service;
	
	@Value("${cl.oneops.birthdate.congratulation}")
    private String CONGRATULATION_MESSAGE;
	
	@Test
    public void returnNormal() throws Exception {
	
        LocalDate now = LocalDate.now();
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setFirstname("person");
        input.setLastname("mcreal");
        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        
        input.setBirthdate(now.format(format));
        
        BirthdayCongratsOutputDto result = service.processUserInfo(input);
        
        assertNotNull(result);
        assertTrue(result.getFirstname().equals("Person"));
        assertNull(result.getCongratulation());
        
	}
	
	@Test
    public void returnBirthdate() throws Exception {
	
		LocalDate date = LocalDate.now();
        date = date.minusYears(1);
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setFirstname("person totaly");
        input.setLastname("mcreal");
        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        
        input.setBirthdate(date.format(format));
        
        BirthdayCongratsOutputDto result = service.processUserInfo(input);
        
        assertNotNull(result);
        assertTrue(result.getFirstname().equals("Person"));
        assertTrue(result.getCongratulation().equals(CONGRATULATION_MESSAGE));
        
	}
	
	@Test
    public void returnFirstnameEmpty() throws Exception {
	
		LocalDate date = LocalDate.now();
        date = date.minusYears(1);
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setLastname("mcreal");
        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        
        input.setBirthdate(date.format(format));
        
        assertThatThrownBy(() -> this.service.processUserInfo(input))
        	.isInstanceOf(BusinessLogicException.class);
        
	}
	
	@Test
    public void returnLastnameEmpty() throws Exception {
	
		LocalDate date = LocalDate.now();
        date = date.minusYears(1);
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setFirstname("mcreal");
        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        
        input.setBirthdate(date.format(format));
        
        assertThatThrownBy(() -> this.service.processUserInfo(input))
        	.isInstanceOf(BusinessLogicException.class);
        
	}
	
	@Test
    public void returnBirthdateEmpty() throws Exception {
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setFirstname("mcreal");
        input.setLastname("mcreal");
        
        assertThatThrownBy(() -> this.service.processUserInfo(input))
        	.isInstanceOf(BusinessLogicException.class);
        
	}
	
	@Test
    public void returnDateFutureEmpty() throws Exception {
	
		LocalDate date = LocalDate.now();
        date = date.plusDays(1);
        BirthdayCongratsInputDto input = new BirthdayCongratsInputDto();
        
        input.setFirstname("mcreal");
        input.setLastname("mcreal");
        
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        
        input.setBirthdate(date.format(format));
        
        assertThatThrownBy(() -> this.service.processUserInfo(input))
        	.isInstanceOf(BusinessLogicException.class);
        
	}
}
