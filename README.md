# BirthdayPoemApi
Test simple spring-boot api
## Requirements
- JDK 8
- Maven 3
## Development
1. Run `mvn install` for update maven dependencies
2. Run `mvn sprig-boot:run` for dev server. Api run on [http://localhost:8090/](http://localhost:8090/)
- Run `mvn test` for running Unit Tests
## Documentation
When api is running you can see methods and examples on [http://localhost:8090/swagger-ui.html](http://localhost:8090/swagger-ui.html)
## Methods
- **Check birthday**
  - **Type**: POST  
  - **uri**: /user/birthdate
  - **Json Body**:  {"firstname" : "firstname", "lastname" : "lastname", "birthdate" : "28-11-1991"}
